# users

[![Maintainer](https://img.shields.io/badge/maintained%20by-labotekh-e00000?style=flat-square)](https://gilab.com/labotekh)
[![License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/labotekh/iac/roles/users/-/blob/main/LICENSE)
[![Release](https://gitlab.com/labotekh/iac/roles/users/-/badges/release.svg)](https://gitlab.com/labotekh/iac/roles/users/-/releases)[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.17-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)

Setup all the users on the system

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Ubuntu | 24.04.1 |

## ⚠️ Requirements

Ansible >= 2.17.

## ⚡ Installation

### Install with git

If you do not want a global installation, clone it into your `roles_path`.

```bash
git clone https://gitlab.com/labotekh/iac/roles/users.git  users
```

But I often add it as a submodule in a given `playbook_dir` repository.

```bash
git submodule add https://gitlab.com/labotekh/iac/roles/users.git roles/users
```

### ✏️ Example Playbook

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: users
      vars:
        admin_group: xxxxxxxxxx
        ansible_password: xxxxxxxxxx
        ansible_username: xxxxxxxxxx
        root_username: xxxxxxxxxx
        runner_password: xxxxxxxxxx
        runner_username: xxxxxxxxxx
        ssh_key_path: '{{ lookup(''env'',''HOME'') }}/.ssh/'
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables

#### main

Setup all the users on the system

| Variable Name | Required | Type | Default | Elements | Description |
|---------------|----------|------|---------|----------|-------------|
| admin_group | True | str |  |  | The name of the admin group |
| root_username | True | str |  |  | The name of the root user |
| ansible_username | True | str |  |  | The name of the ansible user |
| ansible_password | True | str |  |  | The password of the ansible user |
| runner_username | True | str |  |  | The name of the runner user |
| runner_password | True | str |  |  | The password of the runner user |
| ssh_key_path | False | str | {{ lookup('env','HOME') }}/.ssh/ |  | The location of the SSH keys |

## Author Information

Imotekh <imotekh@protonmail.com>